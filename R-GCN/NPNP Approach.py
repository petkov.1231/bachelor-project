from rdflib import Graph
import pandas as pd
import numpy as np
import torch
from torch import nn
from torch.nn.modules.module import Module
import torch.nn.functional as F
import torch.optim as optim
from collections import Counter
from sklearn import preprocessing

class R_GraphConvolution(Module):
    def __init__(self, in_features, out_features, num_rels):
        super(R_GraphConvolution, self).__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.num_rels = num_rels

        self.weight = nn.Parameter(torch.Tensor(self.num_rels, self.in_features, self.out_features).to(cuda))
        nn.init.xavier_uniform_(self.weight)


    def forward(self, prev_layer, adjacency_3d):
        output = 0
        for i, adj in enumerate(adjacency_3d):
            adj = torch.spmm(adj, prev_layer)
            adj = torch.spmm(adj, self.weight[i])
            output = output + adj

        return output


class R_Markov(Module):
    def __init__(self):
        super(R_Markov, self).__init__()

    def forward(self, output):
        par = list()
        for name, param in model.named_parameters():
            if param.requires_grad:
                par.append([name, param.data])
        w = par[1][1].clone()

        ind = list()
        ind1 = list()
        val = list()

        for x in range(num_nodes):
            temp = torch.zeros(16, 16).to(cuda)

            for y in range(len(np_adj[x])):
                if len(np_adj[x][y]) > 2:
                    temp1 = 0
                    for z in range(len(np_adj[x][y]) - 1):
                        temp1 += w[np_adj[x][y][z + 1]]
                    temp = torch.cat((temp, temp1), 0)

                else:
                    temp = torch.cat((temp, w[np_adj[x][y][1]]), 0)

            temp = temp.narrow(0, 16, int(temp.shape[0]) - 16)
            temp -= temp.min(0, keepdim=True)[0]
            temp = F.normalize(temp, p=1, dim=0)

            for i, y in enumerate(np_adj[x]):
                ind.extend(list(map(lambda z: z + y[0] * 16, rows)))
                ind1.extend(list(map(lambda z: z + x * 16, columns)))
                val.extend((temp.narrow(0, i * 16, 16).view(1, 16 * 16)).tolist())

        f = [ind, ind1]
        final = torch.LongTensor(f)
        values = torch.FloatTensor(val).view(transition_matrix_elements * 256)
        transition_matrix = torch.sparse.FloatTensor(final, values, torch.Size([16 * num_nodes, 16 * num_nodes])).to(
            cuda)

        output = output.view(num_nodes * 16, 1)
        output = F.normalize(output, p=1, dim=0)

        for x in range(4):
            output = torch.spmm(transition_matrix, output)
            output = 0.85 * output + 0.15 * (torch.ones([output.shape[0], 1]) / output.shape[0]).to(cuda)

        return output.view(num_nodes, 16)

def m_ReLU (input):
    input[input < 0.1] = 0.1

    return input

class mod_ReLU(nn.Module):
    def __init__(self):

        super().__init__() # init the base class

    def forward(self, input):
        return m_ReLU(input) # simply apply already implemented SiLU


class R_GCN(Module):
    def __init__(self, nfeat, nhid, nclass, num_rels):
        super(R_GCN, self).__init__()

        self.gc1 = R_GraphConvolution(nfeat, nhid, num_rels)
        self.gc2 = R_GraphConvolution(nhid, nhid, num_rels)
        self.gc3 = R_Markov()
        self.gc4 = R_GraphConvolution(nhid, nclass, num_rels)

    def forward(self, x, adj):
        x = F.relu(self.gc1(x, adj))
        x = F.relu(self.gc2(x, adj))
        x = activation_function(self.gc3(x))
        x = self.gc4(x, adj)
        return F.log_softmax(x, dim=1)


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch):
    model.train()
    optimizer.zero_grad()
    output = model(features, sparse)
    loss_train = F.cross_entropy(output[split_train], labels_train)

    acc_train = accuracy(output[split_train], labels_train)
    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))


def test():
    model.eval()
    output = model(features, sparse)
    loss_test = F.cross_entropy(output[split_test], labels_test)
    acc_test = accuracy(output[split_test], labels_test)
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))

def swapPositions(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list


def duplicate(to_swap):
    to_swap = map(tuple, to_swap)
    freqDict = Counter(to_swap)

    for (row, freq) in freqDict.items():
        if freq > 1:
            return True
    return False

rows = list()
for x in range(16):
    rows.extend([x] * 16)

columns = list(range(16)) * 16

g = Graph()
g.parse("data/aifb/aifb_stripped.nt", format="nt")

print(len(g))  # prints 2

data = []
relations, node1, node2 = [], [], []
for a, b, c in g:
    data.append((str(a), str(b), str(c)))
    relations.append(str(b))
    node1.append(str(a))
    node2.append(str(c))

df_train = pd.read_csv('data/aifb/trainingSet.tsv', sep = '\t')
df_test = pd.read_csv('data/aifb/testSet.tsv', sep = '\t')
df_full = pd.read_csv('data/aifb/completeDataset.tsv', sep = '\t')

num_nodes = len(set(node1).union(set(node2)))
num_relations = len(set(relations))
identity_relations = list(set(relations))
identity_nodes = list(set(node1).union(set(node2)))
num_hidden_neurons = 16

cuda = torch.device("cuda" if torch.cuda.is_available() else "cpu")

features = np.random.randint(2, size=(num_nodes, num_hidden_neurons))
features = torch.from_numpy(features).float()
features = features.to(cuda)

count = 0
for x in df_full.person:
    swapPositions(identity_nodes, count, identity_nodes.index(x))
    count += 1
print(duplicate(features))

np_adj = [[] for i in range(num_nodes)]
adjacency = np.zeros((num_relations * 2, num_nodes, 0))
adjacency = adjacency.tolist()
for i, x in enumerate(data):
    adjacency[identity_relations.index(x[1])][identity_nodes.index(x[2])].append(identity_nodes.index(x[0]))
    adjacency[identity_relations.index(x[1]) + len(identity_relations)][identity_nodes.index(x[0])].append(
        identity_nodes.index(x[2]))

    try:
        b = [j[0] for j in np_adj[identity_nodes.index(x[0])]].index(identity_nodes.index(x[2]))
        c = [j[0] for j in np_adj[identity_nodes.index(x[2])]].index(identity_nodes.index(x[0]))
    except ValueError:
        np_adj[identity_nodes.index(x[0])].append([identity_nodes.index(x[2]), identity_relations.index(x[1])])
        np_adj[identity_nodes.index(x[2])].append(
            [identity_nodes.index(x[0]), (identity_relations.index(x[1]) + len(identity_relations))])
    else:
        np_adj[identity_nodes.index(x[0])][b].append(identity_relations.index(x[1]))
        np_adj[identity_nodes.index(x[2])][c].append(identity_relations.index(x[1]) + len(identity_relations))

for x in range(num_nodes):
    try:
        b = [j[0] for j in np_adj[x]].index(x)
    except ValueError:
        np_adj[x].append([x, (num_relations * 2)])
    else:
        np_adj[x][b].append(x)

rel = list()
indence = list()
indence1 = list()
val = list()
val_degree = list()

for m in range(num_relations * 2):
    for i, x in enumerate(adjacency[m]):
        if len(x) > 0:
            for y in x:
                rel.append(m)
                indence.append(i)
                indence1.append(y)
                val.append(1 / len(x))  # normalized
                val_degree.append(1)

for i in range(num_nodes):
    rel.append(num_relations * 2)
    indence.append(i)
    indence1.append(i)  # adding self-loop adjacency matrix as last relation
    val.append(1)

inn = torch.tensor([rel, indence, indence1], device=cuda)
val = torch.tensor(val, device=cuda)
val_degree = torch.tensor(val_degree, device=cuda)

sparse = torch.sparse.FloatTensor(inn, val, torch.Size([num_relations * 2 + 1, num_nodes, num_nodes])).to(cuda)
node_degree = torch.sparse.sum(sparse, dim=[0, 2]).to_dense().to(cuda)

transition_matrix_elements = torch.sparse.sum(sparse,dim = 0)._nnz()

activation_function = mod_ReLU()

labels_train = df_train['label_affiliation']
labels_test = df_test['label_affiliation']
le = preprocessing.LabelEncoder()
le.fit(labels_train.values)
labels_train = torch.tensor(le.transform(labels_train.values), dtype=torch.long, device=cuda)
le.fit(labels_test.values)
labels_test = torch.tensor(le.transform(labels_test.values), dtype=torch.long, device=cuda)

split_train = df_train['id'].values.astype(int)
split_test = df_test['id'].values.astype(int)
split_train = torch.cuda.LongTensor(split_train).squeeze()
split_test = torch.cuda.LongTensor(split_test)

split_train = split_train - 1
split_test = split_test - 1

model = R_GCN(nfeat=features.shape[1], nhid=num_hidden_neurons, nclass=4, num_rels=num_relations * 2 + 1)
optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

for epoch in range(50):
    train(epoch)
test()