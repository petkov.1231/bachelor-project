from rdflib import Graph
import pandas as pd
import numpy as np

import torch
from torch import nn
from torch.nn import Module,Sequential,Linear,ReLU,Dropout,LogSoftmax
from torch.nn.modules.module import Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
import torch.optim as optim
from collections import Counter
from sklearn import preprocessing
from torch_geometric.utils.sparse import dense_to_sparse as ds


def swapPositions(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list


def duplicate(to_swap):
    to_swap = map(tuple, to_swap)
    freqDict = Counter(to_swap)

    for (row, freq) in freqDict.items():
        if freq > 1:
            return True
    return False


class R_Markov(Module):
    def __init__(self, depth, teleport_prob):
        super(R_Markov, self).__init__()

        self.depth = depth
        self.teleport_prob = teleport_prob

    def forward(self, prev_layer, adjacency_3d):
        par = list()
        for name, param in model.named_parameters():
            if param.requires_grad:
                par.append([name, param.data])

        copy_weight = par[0][1].clone()
        copy_weight -= copy_weight.min(2, keepdim=True)[0]  # each row of each weight matrix sums up to 1
        copy_weight = F.normalize(copy_weight, p=1, dim=2)

        list_states = []
        list_states.append(prev_layer)

        for x in range(self.depth):
            output = 0
            for i, adj in enumerate(adjacency_3d):
                adj = torch.spmm(adj, list_states[x])

                temp = adj.clone()
                temp = temp.view(temp.shape[0] * temp.shape[1])  # normalize copy of prev_state
                temp = F.normalize(temp, p=1, dim=0)  # When summing all elements results should be 1
                temp = temp.view(adj.shape[0], adj.shape[1])

                adj = torch.spmm(temp, copy_weight[i])

                output = output + adj

            list_states.append(self.teleport_prob * output / (num_relations * 2 + 1) + (1 - self.teleport_prob) \
                               * torch.Tensor(prev_layer.shape[0], prev_layer.shape[1]) \
                               .fill_(1 / (prev_layer.shape[0] * prev_layer.shape[1])).to(cuda))

        return (nn.L1Loss()(list_states[1], list_states[-1])) * 1000000


class R_GraphConvolution(Module):
    def __init__(self, in_features, out_features, num_rels):
        super(R_GraphConvolution, self).__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.num_rels = num_rels

        self.weight = nn.Parameter(torch.Tensor(self.num_rels, self.in_features, self.out_features).to(cuda))
        nn.init.xavier_uniform_(self.weight)

    def forward(self, prev_layer, adjacency_3d):
        output = 0
        for i, adj in enumerate(adjacency_3d):
            adj = torch.spmm(adj, prev_layer)
            adj = torch.spmm(adj, self.weight[i])
            output = output + adj

        return output


class R_GCN(Module):
    def __init__(self, nfeat, nhid, nclass, num_rels, depth, teleport_prob):
        super(R_GCN, self).__init__()

        self.gc1 = R_GraphConvolution(nfeat, nhid, num_rels)
        self.gc2 = R_Markov(depth, teleport_prob)
        self.gc3 = R_GraphConvolution(nhid, nclass, num_rels)

    def forward(self, x, adj):
        x = F.relu(self.gc1(x, adj))

        l2_loss = self.gc2(x, adj)

        x = self.gc3(x, adj)
        return F.log_softmax(x, dim=1), l2_loss


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch, loss_distribution):
    model.train()
    optimizer.zero_grad()
    output, l2_loss = model(features, sparse)
    l1_loss = F.cross_entropy(output[split_train], labels_train)
    loss_train = loss_distribution * l1_loss + (1 - loss_distribution) * l2_loss
    acc_train = accuracy(output[split_train], labels_train)
    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'l1_train: {:.4f}'.format(l1_loss.item()),
          'l2_train: {:.4f}'.format(l2_loss.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))

    return output


def test():
    model.eval()
    output, l2_loss = model(features, sparse)
    loss_test = F.cross_entropy(output[split_test], labels_test)
    acc_test = accuracy(output[split_test], labels_test)
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))
    return acc_test

g = Graph()
g.parse("data/aifb/aifb_stripped.nt", format="nt")

print(len(g)) # prints 2

data = []
relations, node1, node2 = [], [], []
for a, b, c in g:
    data.append((str(a), str(b), str(c)))
    relations.append(str(b))
    node1.append(str(a))
    node2.append(str(c))

df_train = pd.read_csv('data/aifb/trainingSet.tsv', sep = '\t')
df_test = pd.read_csv('data/aifb/testSet.tsv', sep = '\t')
df_full = pd.read_csv('data/aifb/completeDataset.tsv', sep = '\t')

num_nodes = len(set(node1).union(set(node2)))
num_relations = len(set(relations))
identity_relations = list(set(relations))
identity_nodes = list(set(node1).union(set(node2)))
num_hidden_neurons = 16


cuda = torch.device("cuda" if torch.cuda.is_available() else "cpu")
features = np.random.randint(2, size=(num_nodes, num_hidden_neurons))
features = torch.from_numpy(features).float()
features = features.to(cuda)

count = 0
for x in df_full.person:
    swapPositions(identity_nodes, count, identity_nodes.index(x))
    count += 1
print(duplicate(features))

adjacency = np.zeros((num_relations * 2, num_nodes, 0))
adjacency = adjacency.tolist()
for i, x in enumerate(data):
    adjacency[identity_relations.index(x[1])][identity_nodes.index(x[2])].append(identity_nodes.index(x[0]))
    adjacency[identity_relations.index(x[1]) + len(identity_relations)][identity_nodes.index(x[0])].append(identity_nodes.index(x[2]))

rel = list()
indence = list()
indence1 = list()
val = list()
val_degree = list()

for m in range(num_relations * 2):
    for i, x in enumerate(adjacency[m]):
        if len(x) > 0:
            for y in x:
                rel.append(m)
                indence.append(i)
                indence1.append(y)
                val.append(1 / len(x))  # normalized
                val_degree.append(1)

for i in range(num_nodes):
    rel.append(num_relations * 2)
    indence.append(i)
    indence1.append(i)  # adding self-loop adjacency matrix as last relation
    val.append(1)

inn = torch.tensor([rel, indence, indence1], device=cuda)
val = torch.tensor(val, device=cuda)
val_degree = torch.tensor(val_degree, device=cuda)

sparse = torch.sparse.FloatTensor(inn, val, torch.Size([num_relations * 2 + 1, num_nodes, num_nodes])).to(cuda)
node_degree = torch.sparse.sum(sparse, dim=[0, 2]).to_dense().to(cuda)

labels_train = df_train['label_affiliation']
labels_test = df_test['label_affiliation']
le = preprocessing.LabelEncoder()
le.fit(labels_train.values)
labels_train = torch.tensor(le.transform(labels_train.values), dtype=torch.long, device=cuda)
le.fit(labels_test.values)
labels_test = torch.tensor(le.transform(labels_test.values), dtype=torch.long, device=cuda)

split_train = df_train['id'].values.astype(int)
split_test = df_test['id'].values.astype(int)
split_train = torch.cuda.LongTensor(split_train).squeeze()
split_test = torch.cuda.LongTensor(split_test)

split_train = split_train - 1
split_test = split_test - 1

model = R_GCN(nfeat=features.shape[1], nhid=num_hidden_neurons, nclass=4, num_rels=num_relations * 2 + 1, depth = 3, teleport_prob = 0.85)
optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

for epoch in range(50):
    output = train(epoch, 0.99)
    #if epoch % 10 == True:
    #test()
test()