from rdflib import Graph
import pandas as pd
import numpy as np

import torch
from torch import nn
from torch.nn.modules.module import Module
import torch.nn.functional as F
import torch.optim as optim
from sklearn import preprocessing


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch):
    model.train()
    optimizer.zero_grad()
    output = model(features, sparse)
    loss_train = F.cross_entropy(output[split_train], labels_train)
    acc_train = accuracy(output[split_train], labels_train)
    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))

    return output


def test():
    model.eval()
    output = model(features, sparse)
    loss_test = F.cross_entropy(output[split_test], labels_test)
    acc_test = accuracy(output[split_test], labels_test)
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))

    return acc_test


class R_GCN(Module):
    def __init__(self, nfeat, nhid, nclass, num_rels):
        super(R_GCN, self).__init__()

        self.gc1 = R_GraphConvolution(nfeat, nhid, num_rels, 1)
        self.gc2 = R_GraphConvolution(nhid, nhid, num_rels, 3)
        self.gc3 = R_GraphConvolution(nhid, nclass, num_rels, 1)

    def forward(self, x, adj):
        x = F.relu(self.gc1(x, adj))
        x = F.relu(self.gc2(x, adj))
        x = self.gc3(x, adj)
        return F.log_softmax(x, dim=1)


class R_GraphConvolution(Module):
    def __init__(self, in_features, out_features, num_rels, depth):
        super(R_GraphConvolution, self).__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.num_rels = num_rels
        self.depth = depth

        self.weight = nn.Parameter(torch.Tensor(self.num_rels, self.in_features, self.out_features).to(cuda))
        nn.init.xavier_uniform_(self.weight)

    def forward(self, prev_layer, adjacency_3d):
        output = 0
        for i, adj in enumerate(adjacency_3d):
            adj = torch.spmm(adj, prev_layer)
            adj = torch.spmm(adj, self.weight[i])
            output = output + adj

        if self.depth > 1:
            output = F.relu(output)

            for x in range(self.depth - 1):
                output1 = 0
                for i, adj in enumerate(adjacency_3d):
                    adj = torch.spmm(adj, output)
                    adj = torch.spmm(adj, self.weight[i])
                    output1 = output1 + adj
                output = output1.clone()
                output = F.relu(output)

        return output


def swapPositions(list, pos1, pos2):
    list[pos1], list[pos2] = list[pos2], list[pos1]
    return list


cuda = torch.device("cuda" if torch.cuda.is_available() else "cpu")

g = Graph()
g.parse("data/aifb/aifb_stripped.nt", format="nt")

data = []
relations, node1, node2 = [], [], []
for a, b, c in g:
    data.append((str(a), str(b), str(c)))
    relations.append(str(b))
    node1.append(str(a))
    node2.append(str(c))

df_train = pd.read_csv('data/aifb/trainingSet.tsv', sep='\t')
df_test = pd.read_csv('data/aifb/testSet.tsv', sep='\t')
df_full = pd.read_csv('data/aifb/completeDataset.tsv', sep='\t')

num_nodes = len(set(node1).union(set(node2)))
num_relations = len(set(relations))

identity_relations = list(set(relations))
identity_nodes = list(set(node1).union(set(node2)))

count = 0
for x in df_full.person:
    swapPositions(identity_nodes, count, identity_nodes.index(x))
    count += 1

adjacency = np.zeros((num_relations * 2, num_nodes, 0))
adjacency = adjacency.tolist()
for i, x in enumerate(data):
    adjacency[identity_relations.index(x[1])][identity_nodes.index(x[0])].append(identity_nodes.index(x[2]))
    adjacency[identity_relations.index(x[1]) + len(identity_relations)][identity_nodes.index(x[2])].append(
        identity_nodes.index(x[0]))

rel = list()
indence = list()
indence1 = list()
val = list()

for m in range(num_relations * 2):
    for i, x in enumerate(adjacency[m]):
        if len(x) > 0:
            for y in x:
                rel.append(m)
                indence.append(i)
                indence1.append(y)
                val.append(1 / len(x))  # normalized

for i in range(num_nodes):
    rel.append(num_relations * 2)
    indence.append(i)
    indence1.append(i)  # adding self-loop adjacency matrix as last relation
    val.append(1)

inn = torch.tensor([rel, indence, indence1], device=cuda)
val = torch.tensor(val, device=cuda)
sparse = torch.sparse.FloatTensor(inn, val, torch.Size([num_relations * 2 + 1, num_nodes, num_nodes])).to(cuda)

labels_train = df_train['label_affiliation']
labels_test = df_test['label_affiliation']
le = preprocessing.LabelEncoder()
le.fit(labels_train.values)
labels_train = torch.tensor(le.transform(labels_train.values), dtype=torch.long, device=cuda)
le.fit(labels_test.values)
labels_test = torch.tensor(le.transform(labels_test.values), dtype=torch.long, device=cuda)

split_train = df_train['id'].values.astype(int)
split_test = df_test['id'].values.astype(int)
split_train = torch.cuda.LongTensor(split_train).squeeze()
split_test = torch.cuda.LongTensor(split_test)

split_train = split_train - 1
split_test = split_test - 1

accuracy_test_list = list()

for x in range(50):
    features = np.random.randint(2, size=(num_nodes, 16))
    features = torch.from_numpy(features).float()
    features = features.to(cuda)

    model = R_GCN(nfeat=features.shape[1], nhid=16, nclass=4, num_rels=num_relations * 2 + 1)
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

    for epoch in range(50):
        train(epoch)
    accuracy_test_list.append(test())

    print(x)

print(accuracy_test_list)
print(sum(accuracy_test_list) / 50)

