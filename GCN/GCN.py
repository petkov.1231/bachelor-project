import math
from torch.nn.modules.module import Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
import torch.optim as optim
from dgl.data import citation_graph as citegrh
import torch
from dgl import DGLGraph


class GraphConvolution(Module):

    def __init__(self, in_features, out_features):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(in_features, out_features).to(cuda))
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)

    def forward(self, prev_layer, norm_adjacency):
        support = torch.mm(prev_layer, self.weight)
        return torch.mm(norm_adjacency, support)


class GCN(Module):
    def __init__(self, nfeat, nhid, nclass):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, nhid)
        self.gc2 = GraphConvolution(nhid, nclass)

    def forward(self, x, adj):
        x = self.gc1(x, adj)
        x = F.relu(x)

        x = self.gc2(x, adj)
        return F.log_softmax(x, dim=1)


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch):
    model.train()
    optimizer.zero_grad()
    output = model(features, feedforward)
    loss_train = F.cross_entropy(output[split_train], target[split_train])
    acc_train = accuracy(output[split_train], target[split_train])

    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))


def test():
    model.eval()
    output = model(features, feedforward)
    loss_test = F.cross_entropy(output[split_test], target[split_test])
    acc_test = accuracy(output[split_test], target[split_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))
    return acc_test

cuda = torch.device("cuda" if torch.cuda.is_available() else "cpu")

data = citegrh.load_cora()
features = torch.FloatTensor(data.features).to(cuda)
labels = torch.LongTensor(data.labels).to(cuda)
train_mask = torch.BoolTensor(data.train_mask).to(cuda)
test_mask =torch.BoolTensor(data.test_mask).to(cuda)
g = DGLGraph(data.graph)

adjacency = g.adjacency_matrix()
adjacency = torch.add(torch.eye(g.number_of_nodes()), adjacency)

degree = torch.diag(torch.sum(adjacency, dim=0))

norm_degree = torch.rsqrt(degree)
norm_degree[norm_degree == float("Inf")] = 0

temp = torch.mm(norm_degree, adjacency)
feedforward = torch.mm(temp, norm_degree).to(cuda)
target = labels

split_train = range(140)
split_test = range(140, g.number_of_nodes())
split_train = torch.LongTensor(split_train)
split_test = torch.LongTensor(split_test)


accuracies = list()
for x in range(100):
    model = GCN(nfeat=features.size(1),
                nhid=16,
                nclass=len(set(labels.tolist())))
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

    for epoch in range(200):
        train(epoch)

    accuracies.append(test())

    print(x)

print(min(accuracies))
print(max(accuracies))
print(sum(accuracies) / 100)
