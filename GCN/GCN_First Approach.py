import torch
import numpy as np
import pandas as pd
import math
from torch import nn
from torch.nn.modules.module import Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
import torch.optim as optim
from sklearn import preprocessing
from dgl.data import citation_graph as citegrh
import torch
from dgl import DGLGraph
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches


class R_Markov(Module):
    def __init__(self, depth, teleport_prob):
        super(R_Markov, self).__init__()

        self.depth = depth
        self.teleport_prob = teleport_prob

    def forward(self, prev_layer, norm_adjacency):
        par = list()
        for name, param in model.named_parameters():
            if param.requires_grad:
                par.append([name, param.data])
        copy_weight = par[1][1].clone()

        copy_weight -= copy_weight.min(1, keepdim=True)[0]
        copy_weight = F.normalize(copy_weight, p=1, dim=1)

        list_states = []
        list_states.append(prev_layer)

        for x in range(self.depth):
            support = torch.mm(norm_adjacency, list_states[x])

            temp = support.clone()
            temp = temp.view(support.shape[0] * support.shape[1])  # normalize copy of prev_state
            temp = F.normalize(temp, p=1, dim=0)  # When summing all elements results should be 1
            temp = temp.view(support.shape[0], support.shape[1])

            output = torch.mm(temp, copy_weight)

            list_states.append(self.teleport_prob * output + (1 - self.teleport_prob) \
                               * torch.Tensor(prev_layer.shape[0], prev_layer.shape[1]) \
                               .fill_(1 / (prev_layer.shape[0] * prev_layer.shape[1])))

        return (nn.L1Loss()(list_states[1], list_states[-1])) * 500000


class GraphConvolution(Module):

    def __init__(self, in_features, out_features):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(in_features, out_features))

        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)

    def forward(self, prev_layer, norm_adjacency):
        support = torch.mm(prev_layer, self.weight)
        output = torch.mm(norm_adjacency, support)

        return output


class GCN(Module):
    def __init__(self, nfeat, nhid, nclass, depth, teleport_prob):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, nhid)
        self.gc2 = GraphConvolution(nhid, nhid)

        self.gc3 = R_Markov(depth, teleport_prob)
        self.gc4 = GraphConvolution(nhid, nclass)

    def forward(self, x, adj):
        x = self.gc1(x, adj)
        x = F.relu(x)

        x = self.gc2(x, adj)
        x = F.relu(x)

        l2_loss = self.gc3(x, adj)

        x = self.gc4(x, adj)
        return F.log_softmax(x, dim=1), l2_loss


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch, loss_distribution):
    model.train()
    optimizer.zero_grad()
    output, l2_loss = model(features, feedforward)

    l1_loss = F.nll_loss(output[split_train], target[split_train])

    loss_train = loss_distribution * l1_loss + (1 - loss_distribution) * l2_loss

    acc_train = accuracy(output[split_train], target[split_train])
    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'l1_loss: {:.4f}'.format(l1_loss.item()),
          'l2_loss: {:.4f}'.format(l2_loss.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))

    return l1_loss, l2_loss, acc_train


def test():
    model.eval()
    output, l2_loss = model(features, feedforward)
    loss_test = F.nll_loss(output[split_test], target[split_test])
    acc_test = accuracy(output[split_test], target[split_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))

    return acc_test

data = citegrh.load_citeseer()
features = torch.FloatTensor(data.features)
labels = torch.LongTensor(data.labels)
train_mask = torch.BoolTensor(data.train_mask)
test_mask =torch.BoolTensor(data.test_mask)
g = DGLGraph(data.graph)
num_nodes = g.number_of_nodes()

adjacency = g.adjacency_matrix()
adjacency = torch.add(torch.eye(num_nodes), adjacency)

degree = torch.diag(torch.sum(adjacency, dim=0))

norm_degree = torch.rsqrt(degree)
norm_degree[norm_degree == float("Inf")] = 0

temp = torch.mm(norm_degree, adjacency)
feedforward = torch.mm(temp, norm_degree)

target = labels

split_train = range(120)
split_test = range(120, num_nodes)
split_train = torch.LongTensor(split_train)
split_test = torch.LongTensor(split_test)

running_l1_loss_100 = list()
running_l2_loss_100 = list()
accuracy_train_100 = list()

running_l1_loss_99 = list()
running_l2_loss_99 = list()
accuracy_train_99 = list()

running_l1_loss_995 = list()
running_l2_loss_995 = list()
accuracy_train_995 = list()

running_l1_loss_999 = list()
running_l2_loss_999 = list()
accuracy_train_999 = list()

running_l1_loss_9999 = list()
running_l2_loss_9999 = list()
accuracy_train_9999 = list()

running_l1_loss_0 = list()
running_l2_loss_0 = list()
accuracy_train_0 = list()

acc_list = list()
for x in range(1):
    model = GCN(nfeat=features.size(1),
              nhid=16,
              nclass=len(set(labels.numpy())),
              depth = 3,
              teleport_prob = 0.85)
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

    for epoch in range(200):
        l1_loss, l2_loss, acc = train(epoch, 0.995)
        #running_l1_loss_0.append(l1_loss)
        #running_l2_loss_0.append(l2_loss)
        #accuracy_train_0.append(acc)
    acc_list.append(test())
    print(x)

sum(acc_list) / 1