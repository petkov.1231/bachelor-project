import math
from torch import nn
from torch.nn.modules.module import Module
from torch.nn.parameter import Parameter
import torch.nn.functional as F
import torch.optim as optim
from sklearn import preprocessing
from dgl.data import citation_graph as citegrh
import torch
from dgl import DGLGraph
import time


def m_ReLU(input):
    input[input < 0.1] = 0.1

    return input


class mod_ReLU(nn.Module):
    def __init__(self):
        super().__init__()  # init the base class

    def forward(self, input):
        return m_ReLU(input)  # simply apply already implemented SiLU


activation_function = mod_ReLU()

rows = list()
for x in range(16):
    rows.extend([x] * 16)

columns = list(range(16)) * 16


class GraphConvolution(Module):

    def __init__(self, in_features, out_features):
        super(GraphConvolution, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.FloatTensor(in_features, out_features).to(cuda))
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)

    def forward(self, prev_layer, norm_adjacency):
        support = torch.mm(prev_layer, self.weight)
        return torch.mm(norm_adjacency, support)


class GCN(Module):
    def __init__(self, nfeat, nhid, nclass):
        super(GCN, self).__init__()

        self.gc1 = GraphConvolution(nfeat, nhid)
        self.gc2 = GraphConvolution(nhid, nhid)
        self.gc3 = R_Markov()
        self.gc4 = GraphConvolution(nhid, nclass)

    def forward(self, x, adj):
        x = self.gc1(x, adj)
        x = F.relu(x)

        x = self.gc2(x, adj)
        x = F.relu(x)

        x = activation_function(self.gc3(x))

        x = self.gc4(x, adj)
        return F.log_softmax(x, dim=1)


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def train(epoch):
    model.train()
    optimizer.zero_grad()
    output = model(features, feedforward)
    loss_train = F.nll_loss(output[split_train], target[split_train])
    acc_train = accuracy(output[split_train], target[split_train])

    loss_train.backward()
    optimizer.step()

    print('Epoch: {:04d}'.format(epoch + 1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()))


def test():
    model.eval()
    output = model(features, feedforward)
    loss_test = F.nll_loss(output[split_test], target[split_test])
    acc_test = accuracy(output[split_test], target[split_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))

    return acc_test


class R_Markov(Module):
    def __init__(self):
        super(R_Markov, self).__init__()

    def forward(self, output):

        par = list()
        for name, param in model.named_parameters():
            if param.requires_grad:
                par.append([name, param.data])
        w = par[1][1].clone()

        ind = list()
        ind1 = list()
        val = list()

        output = output.view(num_nodes * 16, 1)
        output = F.normalize(output, p=1, dim=0)

        for z in range(num_nodes):
            nodes = adjacency[:, z].nonzero().view(len(adjacency[:, z].nonzero()))

            w -= w.min(0, keepdim=True)[0]
            w = w * len(nodes)
            w = F.normalize(w, p=1, dim=0) / len(nodes)

            for y in nodes.tolist():
                ind.extend(list(map(lambda x: x + y * 16, rows)))
                ind1.extend(list(map(lambda x: x + z * 16, columns)))
                val.extend((w.view(1, 16 * 16)).tolist())

        temp = [ind, ind1]
        transition_matrix = torch.LongTensor(temp)
        values = torch.FloatTensor(val)
        sparse_transition = torch.sparse.FloatTensor(transition_matrix, values.view(values.shape[0] * values.shape[1]),
                                                     torch.Size([16 * num_nodes, 16 * num_nodes])).to(cuda)

        for x in range(1):
            output = torch.spmm(sparse_transition, output)
            output = 0.85 * output + 0.15 * (torch.ones([output.shape[0], 1]) / output.shape[0]).to(cuda)

        return output.view(num_nodes, 16)

cuda = torch.device("cuda" if torch.cuda.is_available() else "cpu")

data = citegrh.load_citeseer()
features = torch.FloatTensor(data.features).to(cuda)
labels = torch.LongTensor(data.labels).to(cuda)
train_mask = torch.BoolTensor(data.train_mask).to(cuda)
test_mask =torch.BoolTensor(data.test_mask).to(cuda)
g = DGLGraph(data.graph)
num_nodes = g.number_of_nodes()

adjacency = g.adjacency_matrix()
adjacency = torch.add(torch.eye(g.number_of_nodes()), adjacency)

degree = torch.diag(torch.sum(adjacency, dim=0))

norm_degree = torch.rsqrt(degree)
norm_degree[norm_degree == float("Inf")] = 0

temp = torch.mm(norm_degree, adjacency)
feedforward = torch.mm(temp, norm_degree).to(cuda)

target = labels

acc_test = list()

for x in range(1):
    t0 = time.time()

    model = GCN(nfeat=features.size(1),
                nhid=16,
                nclass=len(set(labels.tolist())))
    optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-5)

    split_train = range(120)
    split_test = range(120, num_nodes)
    split_train = torch.LongTensor(split_train)
    split_test = torch.LongTensor(split_test)

    for epoch in range(200):
        t0 = time.time()
        train(epoch)
        print('{} seconds'.format(time.time() - t0))

    acc_test.append(test())

    print(x)

print(sum(acc_test) / 1)
